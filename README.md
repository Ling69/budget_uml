
# Conception & UML - Gestion de budget

  

## Description du projet

  

Pour gerer mon budget, et afin de mieux maîtriser mes dépenses mensuelles, j'ai décidé de créer une petite application de gestion de budget que je vais utiliser dans le cadre de ma famille.

## Analyse de besoin

**Fonctionnalités :**

### QUI?
* client: ma famille
* utilisateur: moi et mon mari

### QUOI?

**obligatoires:**
  * gérer un budget mensuel : entrées et sorties
  * les entrées doivent pouvoir être catégorisées (loisirs, famille, loyer/prêt immobilier, etc)
  * il doit être possible de "geler" une entrée : une entrée gelée n'est plus modifiable
  * l'application doit permettre d'éditer des statistiques mensuelles sur le pourcentage de dépenses par type, l'évolution au fil du temps, etc


**optionnelles:**
  * possibilité de gérer un budget commun, et des budgets séparés
  * possibilité pour les parents de voir le budget de leurs enfants, mais pas l'inverse
  * possibilité de faire un budget prévisionnel, c'est à dire dans le futur, pour planifier des grosses dépenses (vacances, réparation de voiture, achat immobilier, etc)
  * possibilité de voir son solde bancaire et son évolution au fil du temps
  * possibilité d'ajouter des justificatifs (tickets de caisse, ticket de péage, facture, etc)
  *  possibilité de désigner une entrée comme étant récurrente (elle est automatiquement ajoutée tous les mois, par exemple votre abonnement à Internet)


## COMMENT
* context final: sur mon pc
* techno: java/next.js, sql
* délai: le 28 avril

## USAGE
Usage personnel

## Contexte de travail

Le travail est à faire INDIVIDUELLEMENT.

## Éléments de rendu

  * un diagramme de cas d'utilisation complet
  * un modèle de données complet (comment vous stockeriez les données en base), format libre (mcd, extrait de diagramme de classe, etc).
  * un diagramme de classe détaillé : classes, interfaces, héritages éventuels, attributs des classes avec leur visibilité, méthodes des classes avec leur visibilité
  * un fichier Readme détaillant vos choix de conception : le nom des méthodes, pourquoi telle classe, pourquoi telle visibilité, etc
  * **optionnelle** : un logigramme de vos méthodes "importantes"

  ## lien de projet sur draw.io
  https://app.diagrams.net/#Uhttps%3A%2F%2Fgitlab.com%2FLing69%2Fbudget_uml%2F-%2Fraw%2Fmaster%2Fdiagramme%2520_de_class.drawio